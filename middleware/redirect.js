export default ({ redirect, route }) => {
    if (!/\/$/.test(route.path)) {
        redirect(301, route.path + '/');
    }
};
