export const state = () => ({
    pageData: new Map()
})

export const getters = {
    hasPage: (state) => (page) => state.pageData.has(page),
    getPage: (state) => (page) => state.pageData.get(page)
}
export const mutations = {
    setPage(state, { page, data }) {
        state.pageData.set(page, data);
    }
}
export const actions = {
    async getPage({ commit, rootState, getters }, { controller, alias }) {
        const page = rootState.currentLang + '/' + controller + '/' + alias;

        if (getters.hasPage(page) || /_/g.test(controller)) {
            console.log('%cPage already loaded!', 'color: green;')
            return getters.getPage(page);
        } else {

            try {
                const { data } = await this.$axios.get(`/${rootState.currentLang}/v1/rest-ui/` + controller + '/' + alias);
                commit('setPage', { page, data });

                return data;
            } catch ({ response }) {
                return response.data;
            }
        }

    }
}