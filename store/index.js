
export const state = () => ({
    currentLang: null,
    defaultLang: null,
    locales: [],

    globalData: {}
})
// export const getters = {
//     getCurrentLocale:
// }
export const mutations = {
    addLocales: (state, locales) => state.locales = locales,
    setCurretLang: (state, lang) => state.currentLang = lang,
    setDefaultLang: (state, lang) => state.defaultLang = lang,
    setGlobalData: (state, data) => state.globalData = data,
}
import _vue from '~/pages/-.vue';

export const actions = {
    async nuxtServerInit({ commit, state }, { app }) {

        const { data } = await this.$axios.get('/language/language');
        const locales = data.map(l => {
            if (l.is_default) commit('setDefaultLang', l.locale);
            return l.locale;
        })

        // app.router.addRoutes([
        // {
        //     path: `/:lang(${locales.join('|')})?/:alias?`,
        //     component: _vue
        // },
        // {
        //     path: `/:lang(${locales.join('|')})?/:controller?/:alias?`,
        //     component: _vue
        // }])

        commit('addLocales', locales);
    }
}