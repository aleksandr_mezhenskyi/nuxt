
delete process.env['http_proxy'];
delete process.env['HTTP_PROXY'];
delete process.env['https_proxy'];
delete process.env['HTTPS_PROXY'];

import axios from 'axios'
export default {
    router: {
        middleware: 'redirect',
        extendRoutes(routes, resolve) {
            routes.push(
                {
                    path: `/:lang([a-z]{2})?/:alias?`,
                    component: resolve(__dirname, 'pages/-.vue')
                },
                {
                    path: `/:lang([a-z]{2})?/:controller?/:alias?`,
                    component: resolve(__dirname, 'pages/-.vue')
                }
            )
        }


    },

    plugins: ['~/plugins/bootstrap.js'],

    modules: [
        ['@nuxtjs/axios'],
    ],

    axios: {
        debug: false,
        proxy: false,
        baseURL: 'http://api.etf.com.dev11.vintagedev.com.ua'
    }
}