export default {
    async asyncData({ store, params, route, redirect }) {
        let { lang, alias, controller } = params;
        let { path } = route;
        console.log(lang, alias)

        const { defaultLang, locales } = store.state;

        if (lang === defaultLang) {
            redirect(301, route.path.replace(new RegExp(defaultLang + `/?`), ''));
        }
        else if (!lang) {
            lang = defaultLang;
        }

        controller = controller || 'page';
        alias = alias || 'home';
        console.log('Final: ', lang, controller, alias);
        store.commit('setCurretLang', lang);

        return await store.dispatch('data/getPage', { controller, alias });

    }
}